import React from "react";
import Links from "./components/Links";

import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function App() {
  return (
    <div className="container p-4">
      <div className="row">
        <div className="col-md-6 mx-auto">
          <Links />
        </div>
      </div>
      <ToastContainer />
    </div>
  );
}

export default App;
