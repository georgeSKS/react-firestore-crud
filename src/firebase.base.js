import firebase from "firebase/app";
import "firebase/firestore";

/**
 * 1.- Copiar este archivo a firebase.js (no borrar este archivo)
 * 2.- copiar configuracion de tu firebase
 */

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "",
  authDomain: "",
  databaseURL: "",
  projectId: "",
  storageBucket: "",
  messagingSenderId: "",
  appId: "",
};
// Initialize Firebase
const fb = firebase.initializeApp(firebaseConfig);

export const db = fb.firestore();
